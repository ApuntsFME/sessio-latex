\section{Entorns}

En \LaTeX{}, fem servir entorns i comandes per donar format al text. Tots els entorns comencen per
\begin{lstlisting}
    \begin{nomdelentorn}[configuració]
\end{lstlisting}
i acaben amb
\begin{lstlisting}
    \end{nomdelentorn}
\end{lstlisting}
La part de configuració normalment és opcional. També hi ha les comandes, que tenen el format:
\begin{lstlisting}
    \nomdelacomanda{text}
\end{lstlisting}

Podeu comprovar que tot el text d'aquest document es troba dins l'entorn \texttt{document}. Ara veurem alguns entorns útils.

\subsection{Text}

Els títols de les parts del document s'escriuen amb les comandes \texttt{section}, \texttt{subsection}, \texttt{subsubsection} i així successivament. \LaTeX{} els numera automàticament i els posa a l'índex. En documents del tipus \texttt{book}, \texttt{chapter} és un nivell superior.

Es pot donar format al text manualment amb \emph{emph} i amb \textbf{textbf}, entre d'altres. Tanmateix, normalment \LaTeX{} ja dóna al document el format adequat i no cal fer-los servir. Aquestes comandes no es poden fer servir dins d'expressions matemàtiques.

Els enllaços externs s'escriuen amb la comanda \texttt{href}:
\href{https://apuntsfme.gitlab.io}{ApuntsFME}. \footnote{També podeu posar enllaços a llocs del mateix document, amb \href{https://en.wikibooks.org/wiki/LaTeX/Labels_and_Cross-referencing}{\texttt{label} i \texttt{ref}}.}

Les notes al peu es fan amb \texttt{footnote}.

\subsection{Llistes}
Per fer llistes amb punts farem servir l'entorn \texttt{itemize}:

\begin{itemize}
    \item 500 g de rap
    \item 6 gambes roges
    \item 6 cigales de Gandia
    \item 200 g de tomaca picada
    \item 1 ceba ratllada
    \item \dots
\end{itemize}

Per fer-ne amb nombres o lletres ho farem amb \texttt{enumerate}. Si no configurem res ens la farà amb nombres seguits d'un punt:

\begin{enumerate}
    \item Col·loquem la paella amb l’oli a foc moderat.
    \item Quan l’oli estiga calent, afegim les gambes i les cigales, les sofregim un poc i les reservem.
\end{enumerate}

Si volem fer-la amb lletres, hi afegirem l'opció de configuració \texttt{a} o \texttt{A}, i amb nombres romans, \texttt{i} o \texttt{I}. També podem canviar els punts per altres símbols, aquí en teniu uns exemples:

\begin{enumerate}[a)]
    \item Afegim l’all picat i la ceba ratllada, quan estiguen daurats incorporarem el pebre roig i la tomaca remenant-ho perquè no es creme.
    \item  Afegim el rap, el safrà, donem unes voltes i afegim el fons de peix ben calent (2 litres aproximadament).
\end{enumerate}

\begin{enumerate}[(i)]
    \item Incorporem els fideus i rectifiquem de sal i colorant. És important mantindre els cinc primers minuts a màxima potència.
    \item Aquesta classe de fideus necessiten de 10 a 12 minuts de cocció
\end{enumerate}

\begin{enumerate}[I]
    \item En els últims tres minuts col·loquem les gambes i les cigales per damunt de forma uniforme fins que quede al punt. 
    \item Deixem reposar 5 minuts i ja està a punt per a servir.
\end{enumerate}

\subsection{Taules}

Per fer taules fem servir \texttt{tabular}:
\begin{center}
  \begin{tabular}{ l | c  r } % La configuració indica l'alineació i les línies verticals
    \hline
    u      & dos  & tres \\ \hline % \hline és una línia horitzontal
    quatre & cinc & sis \\
    set    &      & nou \\  % Observem que es poden deixar cel·les buides
    \hline
  \end{tabular}
\end{center}

Fixeu-vos en com està declarada: \verb/\begin{tabular}{ l | c  r }/. L'argument que passem \verb/{ l | c  r }/ declara les columnes (cada lletra representa una columna). La lletra en qüestió indica l'alineació de la columna (\verb|l| és de ``left'', \verb|c| és de ``center'' i \verb|r| és de ``right''). En aquest cas, per tant, el text de la primera columna està alineat a l'esquerra, el de la segona està centrat i a l'última està alineat a la dreta. A més, entre la primera i la segona columna podem veure el caràcter \verb/|/. Aquest caràcter indica que cal posar-hi una línia vertical entre les dues columnes. Podeu provar de substituir aquella línia per quelcom com per exemple \verb/\begin{tabular}{|| c | c ||| c |}/ per veure exactament com funciona (els espais entre les \verb|c| i les \verb/|/ són inncessaris, són per fer més clar el codi).

Si voleu posar línies entre files, ho podeu fer amb la comanda \verb|\hline|, tal com es veu en el codi de l'exemple.

Us pot ser útil el següent link, que genera taules automàticament (però és millor que us acostumeu a picar-les directament!): \url{https://www.tablesgenerator.com/}.

\subsection{Teoremes, demostracions, etc.}

\LaTeX{} permet crear entorns per enunciar teoremes, proposicions, demostracions, etc. Per fer això, cal declarar els entorns que volem utilitzar de la següent manera:

\begin{lstlisting}
\newtheorem{nom_entorn}{nom_al_document}[enumeracio]
\newtheorem{nom_entorn}[enumeracio]{nom_al_document}
\end{lstlisting}

Els dos casos són diferents, els expliquem amb un exemple:

\begin{lstlisting}
\newtheorem{teo}{Teorema}[section]
\newtheorem{prop}{Proposició}[teo]
\newtheorem{lema}[teo]{Lema}
\end{lstlisting}

En primer lloc declarem els teoremes i els enumerem després de les seccions. És a dir, si estem a la secció 1.4, el primer teorema serà el número 1.4.1. En segon lloc declarem les proposicions i les enumerem després dels teoremes, per tant, si l'últim teorema és el 1.4.1, la propera proposició serà al 1.4.1.1. Finalment, hem definit els lemes i els enumerem de la mateixa forma que els teoremes, per tant, si l'últim teorema era el 1.4.1 i posem un lema, aquest últim tindrà el número 1.4.2. Podeu trobar més informació \href{https://www.overleaf.com/learn/latex/Theorems_and_proofs}{aquí}.

Un cop declarats, afegiriem un teorema amb el següent codi:

\begin{lstlisting}
\begin{teo}
    En els triangles rectangles el quadrat del costat oposat a l'angle recte és igual a la suma dels quadrats dels costats que comprenen a l'angle recte.
\end{teo}
\end{lstlisting}

Al pdf veuríem una cosa semblant a

\begin{teo}
    En els triangles rectangles el quadrat del costat oposat a l'angle recte és igual a la suma dels quadrats dels costats que comprenen a l'angle recte.
\end{teo}